package com.example.farid.amandatest_4;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Rating;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.example.farid.amandatest_4.model.pojo.AndroidVer;
import com.example.farid.amandatest_4.model.pojo.Recepies;
import com.example.farid.amandatest_4.model.pojo.Recipe;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataAdapter_Recipes extends RecyclerView.Adapter<DataAdapter_Recipes.ViewHolder> {
    private ArrayList<Recipe> recipes;
    private String recipe_id;


    Context c;

    public DataAdapter_Recipes(ArrayList<Recipe> data) {
        this.recipes = data;
    }

//    public DataAdapter_Recipes(Context c, ArrayList<Recipe> recipes) {
//        this.c = c;
//        this.recipes = recipes;
//    }




    @Override
    public DataAdapter_Recipes.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter_Recipes.ViewHolder viewHolder, int i) {

        float d= (float) ((recipes.get(i).getSocialRank()) /20);

        final String RId = recipes.get(i).getRecipeId();

        Log.e("asal: ", String.valueOf(recipes.get(i).getSocialRank()));
        Log.e("hasil: ", String.valueOf(d));

        viewHolder.recipe_title.setText(recipes.get(i).getTitle());
        viewHolder.recipe_publisher.setText(recipes.get(i).getPublisher());
        viewHolder.recipe_socialrank.setRating(d);

        String image_url = recipes.get(i).getImageUrl();
        Glide.with(viewHolder.recipe_image.getContext())
                .load(image_url)
                .error(R.mipmap.ic_launcher)
                .override(200, 200)
                //.centerCrop()
                .fitCenter()
                .into(viewHolder.recipe_image);





//        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v)
//            {
////                Intent DetailIntent = new Intent(viewHolder.itemView.getContext(),DetailActivity.class);
////                DetailIntent.putExtra("idresep", recipe_id);
////                viewHolder.itemView.getContext().startActivity(DetailIntent);
//               // Log.d("recipe_id: ", recipes.get(i).getRecipeId();
//
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ClickListener clicklistener = null;
//        private TextView recipe_title,recipe_publisher;
//        private RatingBar recipe_socialrank;
//        private ImageView recipe_image;

        @BindView(R.id.recipe_title) TextView recipe_title;
        @BindView(R.id.recipe_publisher) TextView recipe_publisher;
        @BindView(R.id.recipe_socialrank) RatingBar recipe_socialrank;
        @BindView(R.id.recipe_image) ImageView recipe_image;

        @BindView(R.id.main) LinearLayout main;


        public ViewHolder(final View view)  {
            super(view);
            ButterKnife.bind(this, view);
//            recipe_title = (TextView)view.findViewById(R.id.recipe_title);
//            recipe_publisher = (TextView)view.findViewById(R.id.recipe_publisher);
//            recipe_socialrank = (RatingBar) view.findViewById(R.id.recipe_socialrank);
//            recipe_image = (ImageView)itemView.findViewById(R.id.recipe_image);
//
//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v)
//                {
//                    Intent DetailIntent = new Intent(view.getContext(),DetailActivity.class);
//                    DetailIntent.putExtra("idresep", recipe_id);
//                    view.getContext().startActivity(DetailIntent);
//
//                }
//            });

            main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(),"Get Recipe " + recipes.get(getLayoutPosition()).getTitle(), Toast.LENGTH_SHORT).show();

//                    Toast.makeText(itemView.getContext(), "Position:" + Integer.toString(getLayoutPosition()), Toast.LENGTH_SHORT).show();
//                    Log.d("Cek ID: ", recipes.get(getLayoutPosition()).getRecipeId());

                    Intent DetailIntent = new Intent(view.getContext(),DetailActivity.class);
                    DetailIntent.putExtra("idresep", recipes.get(getLayoutPosition()).getRecipeId());
                    view.getContext().startActivity(DetailIntent);

                    if(clicklistener !=null){
                        clicklistener.itemClicked(v,getAdapterPosition());
                    }
                }
            });


        }

        public void setClickListener(ClickListener clicklistener){
            this.clicklistener  = clicklistener;
        }


    }
}
