
package com.example.farid.amandatest_4.model.pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Recipe {

    @SerializedName("publisher")
    @Expose
    private String publisher;
    @SerializedName("f2f_url")
    @Expose
    private String f2fUrl;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("source_url")
    @Expose
    private String sourceUrl;
    @SerializedName("recipe_id")
    @Expose
    private String recipeId;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("social_rank")
    @Expose
    private double socialRank;
    @SerializedName("publisher_url")
    @Expose
    private String publisherUrl;

    /**
     * 
     * @return
     *     The publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * 
     * @param publisher
     *     The publisher
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Recipe withPublisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    /**
     * 
     * @return
     *     The f2fUrl
     */
    public String getF2fUrl() {
        return f2fUrl;
    }

    /**
     * 
     * @param f2fUrl
     *     The f2f_url
     */
    public void setF2fUrl(String f2fUrl) {
        this.f2fUrl = f2fUrl;
    }

    public Recipe withF2fUrl(String f2fUrl) {
        this.f2fUrl = f2fUrl;
        return this;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public Recipe withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * 
     * @return
     *     The sourceUrl
     */
    public String getSourceUrl() {
        return sourceUrl;
    }

    /**
     * 
     * @param sourceUrl
     *     The source_url
     */
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Recipe withSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
        return this;
    }

    /**
     * 
     * @return
     *     The recipeId
     */
    public String getRecipeId() {
        return recipeId;
    }

    /**
     * 
     * @param recipeId
     *     The recipe_id
     */
    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public Recipe withRecipeId(String recipeId) {
        this.recipeId = recipeId;
        return this;
    }

    /**
     * 
     * @return
     *     The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * 
     * @param imageUrl
     *     The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Recipe withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    /**
     * 
     * @return
     *     The socialRank
     */
    public double getSocialRank() {
        return socialRank;
    }

    /**
     * 
     * @param socialRank
     *     The social_rank
     */
    public void setSocialRank(double socialRank) {
        this.socialRank = socialRank;
    }

    public Recipe withSocialRank(double socialRank) {
        this.socialRank = socialRank;
        return this;
    }

    /**
     * 
     * @return
     *     The publisherUrl
     */
    public String getPublisherUrl() {
        return publisherUrl;
    }

    /**
     * 
     * @param publisherUrl
     *     The publisher_url
     */
    public void setPublisherUrl(String publisherUrl) {
        this.publisherUrl = publisherUrl;
    }

    public Recipe withPublisherUrl(String publisherUrl) {
        this.publisherUrl = publisherUrl;
        return this;
    }

}
