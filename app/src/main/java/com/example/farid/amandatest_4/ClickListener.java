package com.example.farid.amandatest_4;

import android.view.View;

public interface ClickListener {
    public void itemClicked(View view , int position);
}
