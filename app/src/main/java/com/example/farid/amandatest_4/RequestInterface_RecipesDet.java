package com.example.farid.amandatest_4;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RequestInterface_RecipesDet {
    @GET("get?")
    Call<JSONResponse_RecipesDet> getJSON(@Query("key") String key, @Query("rId") String rId);

}
