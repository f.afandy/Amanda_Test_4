package com.example.farid.amandatest_4;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.farid.amandatest_4.model.pojo.RecipeDetailByID;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.databinding.DataBindingUtil.setContentView;

public class DetailActivity  extends AppCompatActivity {

//    @BindView(R.id.editText) EditText txtIDRecipe;

    private ArrayList<RecipeDetailByID> dataDetail;
    private GoogleApiClient client;
    private ArrayAdapter<String> listAdapter;

    @BindView(R.id.recipe_title) TextView recipe_title;
    @BindView(R.id.recipe_image) ImageView recipe_image;
    @BindView(R.id.recipe_socialrank) RatingBar recipe_socialrank;
    @BindView(R.id.IngredientView) ListView IngredientView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        Intent intentFromMain = getIntent();
        String tmpID = intentFromMain.getStringExtra("idresep");

        ButterKnife.bind(this);

        loadJSONDet(tmpID);


//        txtIDRecipe.setText(tmpID);
    }

    private void loadJSONDet(String idResep) {
        final ProgressDialog loading = ProgressDialog.show(this, "Get Data Recipe", "Please wait..", false, false);



        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        int proxyPort = 8080;
        String proxyHost = "192.168.10.30";
        final String username = "";
        final String password = "";

//        SocketAddress address = new InetSocketAddress(proxyHost, proxyPort);
//
//        java.net.Proxy proxyTest = new java.net.Proxy(java.net.Proxy.Type.HTTP,address);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .proxy(proxyTest)
                .addNetworkInterceptor(httpLoggingInterceptor)
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

        Retrofit retrofit = new Retrofit.Builder().client(okHttpClient)
                .baseUrl("http://food2fork.com/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RequestInterface_RecipesDet request_det = retrofit.create(RequestInterface_RecipesDet.class);

        Call<JSONResponse_RecipesDet> call = request_det.getJSON("74698d8507204ffb7959a7d00c0d1593",idResep);

        call.enqueue(new Callback<JSONResponse_RecipesDet>() {

            @Override
            public void onResponse(Call<JSONResponse_RecipesDet> call, Response<JSONResponse_RecipesDet> response) {
                loading.dismiss();

                JSONResponse_RecipesDet jsonResponse = response.body();
                dataDetail = new ArrayList<>(Arrays.asList(jsonResponse.getRecipeDet()));

                for (RecipeDetailByID p : dataDetail){
                    recipe_title.setText(p.getTitle());

                    String image_url = p.getImageUrl();
                    float d= (float) ((p.getSocialRank()) /20);

                    recipe_socialrank.setRating(d);

                    Glide.with(recipe_image.getContext())
                            .load(image_url)
                            .error(R.mipmap.ic_launcher)
                            //.override(200, 200)
                            //.centerCrop()
                            .fitCenter()
                            .into(recipe_image);

                    //ingredientRowView = new.ArrayAdapter<String>
//                    ArrayList<String> IngredientList =  new ArrayList<String>();
//                    IngredientList.addAll(Arrays.<String>asList(String.valueOf(p.getIngredients())));

                    listAdapter = new ArrayAdapter<String>(DetailActivity.this, R.layout.ingredient_row,p.getIngredients());
                    IngredientView.setAdapter(listAdapter);
//                    UIUtils.setListViewHeightBasedOnItems(listAdapter);


                }


//                Log.d("Langsung: ", jsonResponse.toString());
//                for (RecipeDetailByID p : dataDetail)
//                    System.out.println("point x: " + p.getTitle());

            }

            @Override
            public void onFailure(Call<JSONResponse_RecipesDet> call, Throwable t) {
                String errorMessage = (t.getMessage() == null) ? "Message is empty" : t.getMessage();
                Log.d("Error:", errorMessage);
                loading.dismiss();
            }
        });

    }
}
