package com.example.farid.amandatest_4;

import com.example.farid.amandatest_4.model.pojo.Recepies;
import com.example.farid.amandatest_4.model.pojo.Recipe;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RequestInterface_Recipes {
    @GET("search?")
    Call<JSONResponse_Recipes> getJSON(@Query("key") String key, @Query("q") String resep, @Query("sort") String sort, @Query("page") String page);

//    @GET("search?key=74698d8507204ffb7959a7d00c0d1593&q=chick&sort=r&page=1")
//    Call<JSONResponse_Recipes> getJSON();

//    @GET("search?")
//    rx.Observable<Recipe>
//    getRecipeData(@Query("key") String key, @Query("q") String resep, @Query("sort") String sort,
//                                         @Query("page") String page);

}

