
package com.example.farid.amandatest_4.model.pojo;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Recepies {

    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("recipes")
    @Expose
    private List<Recipe> recipes = new ArrayList<Recipe>();

    /**
     * 
     * @return
     *     The count
     */
    public int getCount() {
        return count;
    }

    /**
     * 
     * @param count
     *     The count
     */
    public void setCount(int count) {
        this.count = count;
    }

    public Recepies withCount(int count) {
        this.count = count;
        return this;
    }

    /**
     * 
     * @return
     *     The recipes
     */
    public List<Recipe> getRecipes() {
        return recipes;
    }

    /**
     * 
     * @param recipes
     *     The recipes
     */
    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    public Recepies withRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
        return this;
    }

}
