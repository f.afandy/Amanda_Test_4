package com.example.farid.amandatest_4;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.farid.amandatest_4.model.pojo.AndroidVer;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<AndroidVer> android;

    public DataAdapter(ArrayList<AndroidVer> android) {
        this.android = android;
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tv_name.setText(android.get(i).getName());
        viewHolder.tv_version.setText(android.get(i).getVer());
        viewHolder.tv_api_level.setText(android.get(i).getApi());
        viewHolder.image_logo.setImageResource(R.mipmap.ic_launcher);
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_name,tv_version,tv_api_level;
        private ImageView image_logo;
        public ViewHolder(View view) {
            super(view);

            tv_name = (TextView)view.findViewById(R.id.recipe_title);
            tv_version = (TextView)view.findViewById(R.id.recipe_title);
            tv_api_level = (TextView)view.findViewById(R.id.recipe_title);
            image_logo = (ImageView)itemView.findViewById(R.id.recipe_image);

        }
    }
}
