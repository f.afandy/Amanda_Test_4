package com.example.farid.amandatest_4;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestInterface {
    @GET("jsonandroid")
    Call<JSONResponse> getJSON();
}
