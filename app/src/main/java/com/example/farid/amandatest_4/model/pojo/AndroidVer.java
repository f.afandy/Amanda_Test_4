
package com.example.farid.amandatest_4.model.pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class AndroidVer {

    @SerializedName("ver")
    @Expose
    private String ver;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("api")
    @Expose
    private String api;

    /**
     *
     * @return
     *     The ver
     */
    public String getVer() {
        return ver;
    }

    /**
     *
     * @param ver
     *     The ver
     */
    public void setVer(String ver) {
        this.ver = ver;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The api
     */
    public String getApi() {
        return api;
    }

    /**
     *
     * @param api
     *     The api
     */
    public void setApi(String api) {
        this.api = api;
    }

}
