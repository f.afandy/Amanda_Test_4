
package com.example.farid.amandatest_4.model.pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class RecipeDetail {

    @SerializedName("recipe")
    @Expose
    private Recipe recipe;

    /**
     * 
     * @return
     *     The recipe
     */
    public Recipe getRecipe() {
        return recipe;
    }

    /**
     * 
     * @param recipe
     *     The recipe
     */
    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

}
