package com.example.farid.amandatest_4.model.pojo;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.farid.amandatest_4.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataAdapter_Recipes_backUp extends RecyclerView.Adapter<DataAdapter_Recipes_backUp.ViewHolder> {
    private ArrayList<Recipe> recipes;

    public DataAdapter_Recipes_backUp(ArrayList<Recipe> recipes) {
        this.recipes = recipes;
    }

    @Override
    public DataAdapter_Recipes_backUp.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter_Recipes_backUp.ViewHolder viewHolder, int i) {

        float d= (float) ((recipes.get(i).getSocialRank()) /20);

        Log.e("asal: ", String.valueOf(recipes.get(i).getSocialRank()));
        Log.e("hasil: ", String.valueOf(d));

        viewHolder.recipe_title.setText(recipes.get(i).getTitle());
        viewHolder.recipe_publisher.setText(recipes.get(i).getPublisher());
        viewHolder.recipe_socialrank.setRating(d);

        String image_url = recipes.get(i).getImageUrl();
        Glide.with(viewHolder.recipe_image.getContext())
                .load(image_url)
                .error(R.mipmap.ic_launcher)
                .override(200, 200)
                //.centerCrop()
                .fitCenter()
                .into(viewHolder.recipe_image);

    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
//        private TextView recipe_title,recipe_publisher;
//        private RatingBar recipe_socialrank;
//        private ImageView recipe_image;
        @BindView(R.id.recipe_title) TextView recipe_title;
        @BindView(R.id.recipe_publisher) TextView recipe_publisher;
        @BindView(R.id.recipe_socialrank) RatingBar recipe_socialrank;
        @BindView(R.id.recipe_image) ImageView recipe_image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
//            recipe_title = (TextView)view.findViewById(R.id.recipe_title);
//            recipe_publisher = (TextView)view.findViewById(R.id.recipe_publisher);
//            recipe_socialrank = (RatingBar) view.findViewById(R.id.recipe_socialrank);
//            recipe_image = (ImageView)itemView.findViewById(R.id.recipe_image);

        }
    }
}
