package com.example.farid.amandatest_4;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Proxy;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.example.farid.amandatest_4.model.pojo.AndroidVer;
import com.example.farid.amandatest_4.model.pojo.Recepies;
import com.example.farid.amandatest_4.model.pojo.Recipe;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.OkHttpClient;
import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

//    private RecyclerView recyclerView;
    private ArrayList<Recipe> data;
    private DataAdapter_Recipes adapter;

    private SearchView mSearchView;

    private GoogleApiClient client;

    @BindView(R.id.card_recycler_view) RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_main);

        //handleIntent(getIntent());


        initViews();

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

//    protected void onNewIntent(Intent intent) {
//       super.onNewIntent(intent);
//        handleIntent(intent);
//    }




    private void initViews() {
        //recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        loadJSON("chick");
    }

    private void loadJSON(String query) {
        final ProgressDialog loading = ProgressDialog.show(this, "Get Data", "Please wait..", false, false);


        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        int proxyPort = 8080;
        String proxyHost = "192.168.10.30";
        final String username = "";
        final String password = "";

//        SocketAddress address = new InetSocketAddress(proxyHost, proxyPort);
//
//        java.net.Proxy proxyTest = new java.net.Proxy(java.net.Proxy.Type.HTTP,address);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .proxy(proxyTest)
                .addNetworkInterceptor(httpLoggingInterceptor)
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();

////-------------------------------------------------------------------------------------
//        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.create();
//
//
//        Retrofit retrofit = new Retrofit.Builder().client(okHttpClient)
//                .baseUrl("http://food2fork.com/api/")
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .addCallAdapterFactory(rxAdapter)
//                .build();
//
//        RequestInterface_Recipes request = retrofit.create(RequestInterface_Recipes.class);
//
//        Observable<Recipe> RecipeData = request.getRecipeData("74698d8507204ffb7959a7d00c0d1593","chick","r","1");
//        RecipeData.subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                //.<ItemLayoutBinding>bindRecyclerView(recyclerView, R.layout.item_row)
////                .subscribe(viewHolder -> {
////                    ItemLayoutBinding  b = viewHolder.getViewDataBinding();
////                    b.textViewItem.setText(viewHolder.getItem());
////                })
//        ;
//
//
//        List myList = RecipeData.toList().toBlocking().single();
//        data = new ArrayList<>(myList);
//        adapter = new DataAdapter_Recipes(data);
//        recyclerView.setAdapter(adapter);
//
//
//        Log.d("coba :", myList.toString());
//
//
//                //.subscribe(Recipe -> Log.d("Output: ", Recipe));
//
//        loading.dismiss();
//
//
////------------------------------------------------------------------------------

        Retrofit retrofit = new Retrofit.Builder().client(okHttpClient)
                .baseUrl("http://food2fork.com/api/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RequestInterface_Recipes request = retrofit.create(RequestInterface_Recipes.class);

        Call<JSONResponse_Recipes> call = request.getJSON("74698d8507204ffb7959a7d00c0d1593",query,"r","1");

        Log.d("Cek:",call.toString());

        call.enqueue(new Callback<JSONResponse_Recipes>() {

            @Override
            public void onResponse(Call<JSONResponse_Recipes> call, Response<JSONResponse_Recipes> response) {
                loading.dismiss();

                JSONResponse_Recipes jsonResponse = response.body();
                data = new ArrayList<>(Arrays.asList(jsonResponse.getRecipe()));

                Log.d("123: ", jsonResponse.toString());
                Log.d("123: ", data.toString());

                adapter = new DataAdapter_Recipes(data);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<JSONResponse_Recipes> call, Throwable t) {
                String errorMessage = (t.getMessage() == null) ? "Message is empty" : t.getMessage();
                Log.d("Error:", errorMessage);
                loading.dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Main Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.example.farid.amandatest_4/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Main Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.example.farid.amandatest_4/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.search).getActionView();

        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setIconifiedByDefault(true);

        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);

        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        Log.d("cari... ", query);
        loadJSON(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

//    private void handleIntent(Intent intent) {
//        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
//            String query = intent.getStringExtra(SearchManager.QUERY);
////            Toast.makeText(this,"Pencarian: " + query, Toast.LENGTH_SHORT).show();
//            Log.d("cari... ", query);
//        }
//
//    }

    public void itemClicked(View view, int index) {
        Log.d("Posisi: ", Integer.toString(index));

        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("idresep", index);
        startActivity(intent);
    }

    @Override
    public boolean onClose() {
        return false;
    }
}
